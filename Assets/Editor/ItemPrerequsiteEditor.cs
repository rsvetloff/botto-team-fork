﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomPropertyDrawer(typeof(ItemPrerequisite))]
public class ItemPrerequsiteEditor : PropertyDrawer {

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position,label, property);

        position = EditorGUI.PrefixLabel(position,GUIUtility.GetControlID(FocusType.Passive), label);
        EditorGUI.EndProperty();
    }
}
