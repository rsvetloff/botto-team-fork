﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class PlatformTrigger : MonoBehaviour {


    private Collider _trigger;
    private PlatformController _platformController;

	// Use this for initialization
	void Start ()
    {
        _trigger = GetComponent < Collider > ();
        _trigger.isTrigger = true;
	}


    void OnTriggerEnter(Collider other)
    {
        PlatformRider rider = getRider(other);
        Debug.Log("platform:on");
        if(rider!=null)
        {
            rider.transferParent(transform);
        }
    }


    void OnTriggerExit(Collider other)
    {
        PlatformRider rider = getRider(other);
        Debug.Log("platform:off");
        if (rider!=null)
        {
            rider.restParent();
        }
    }
	

    private PlatformRider getRider(Collider riderCollider)
    {
        return riderCollider.gameObject.GetComponentInParent<PlatformRider>();
    }
	
}
