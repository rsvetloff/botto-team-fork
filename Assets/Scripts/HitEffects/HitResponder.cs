﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public abstract class HitResponder : MonoBehaviour {

    public class HitRespondEquator : IEqualityComparer<HitResponder>
    {
        public bool Equals(HitResponder x, HitResponder y)
        {
            return x.gameObject.Equals(y.gameObject);
        }

        public int GetHashCode(HitResponder obj)
        {
            return obj.GetHashCode()+obj.gameObject.GetHashCode();
        }
    }

    public abstract void attributeHit(HitEffect hitEffect);
}
