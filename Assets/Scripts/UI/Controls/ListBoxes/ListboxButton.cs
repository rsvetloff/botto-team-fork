﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ListboxButton : MonoBehaviour
{
    private delegate  void OnClick();
    private OnClick onClick;

    private int _valueIndex=-1;

    private InventoryItemListbox listbox;

    private Text _text;
    
    public int ValueIndex
    {
        get
        {
            return _valueIndex;
        }
        set
        {
            if(_valueIndex <0)
            {
                _valueIndex = value;
            }
        }


    }

    private Button button;

    public Color selected;
    public Color normal;

    void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(() => click());
        button.image.color = normal;
        _text = GetComponentInChildren<Text>();
    }

    public void unRegister()
    {
        listbox.OnValueChange -= Listbox_OnValueChange;
    }

    void Start()
    {
        listbox = GetComponentInParent<InventoryItemListbox>();
        listbox.OnValueChange += Listbox_OnValueChange;
    }
    /// <summary>
    /// this is invoked when the value in the parent box changes.
    /// </summary>
    /// <param name="index"></param>
    private void Listbox_OnValueChange(int index)
    {
        if(index==_valueIndex)
        {
            button.image.color = selected;
        }
        else
        {
            button.image.color = normal;
        }
    }

    void click()
    {
        listbox.setValue(_valueIndex);
    }

    public void setText(string text)
    {
        _text.text = text;
    }
}
