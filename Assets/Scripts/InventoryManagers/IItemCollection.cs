﻿using UnityEngine;
using System.Collections;

public interface IItemCollection  
{

	 int add(InventoryItem item);

	int getCount();

	InventoryItem lookAt(int index);

	void remove(InventoryItem item);

}
