﻿using UnityEngine;
using System.Collections;
using UnityEditor;
[CustomEditor(typeof(GameAttributeManager))]
public class AttributeManagerNEditor : AttributesEditor 
{


	private bool attributesCollapsed;

	private bool modifiersCollapsed;


	public override void OnInspectorGUI()
	{
		DrawDefaultInspector ();
		GameAttributeManager thisManger = (GameAttributeManager)target;
		attributesCollapsed = EditorGUILayout.Foldout (attributesCollapsed, "Managed Attributes");
		if (attributesCollapsed) 
		{
			base.OnInspectorGUI ();
		}

		modifiersCollapsed = EditorGUILayout.Foldout (modifiersCollapsed, "Modifiers");
		if (modifiersCollapsed) 
		{
			float[] mods = thisManger.getModifiers();
			GameAttribute[] attrs = thisManger.getAttributes();
			for(int i=0;i<mods.Length;i++)
			{
				EditorGUILayout.LabelField(attrs[i].getCode().ToString() + "-" + mods[i]);
			}
		}

		if (GUILayout.Button ("Collate")) 
		{
			thisManger.collate();
		}

	}


}
