﻿using UnityEngine;
using System.Collections;

public class Grid2DMap : MonoBehaviour, ISerializationCallbackReceiver
{
    public int[,,] mapLayout;

    [HideInInspector]
    [SerializeField]
    private int[] m_FlattendMapLayout;

    [HideInInspector]
    [SerializeField]
    private int m_FlattendMapLayoutRows;

    [HideInInspector]
    [SerializeField]
    private int m_FlattendMapLayoutCols;

    public void OnBeforeSerialize()
    {
        int c1 = mapLayout.GetLength(0);
        int c2 = mapLayout.GetLength(1);
        int c3 = mapLayout.GetLength(2);
        int count = c1 * c2*c3;
        m_FlattendMapLayout = new int[count];
        m_FlattendMapLayoutRows = c1;
        m_FlattendMapLayoutCols = c2;
        for (int j = 0; j < c3; j++)
        {
            for (int i = 0; i < count; i++)
            {
                m_FlattendMapLayout[i+(j* c2*c1)] = mapLayout[i % c1, i / c1,j];
            }
        }
    }
    public void OnAfterDeserialize()
    {
        int count = m_FlattendMapLayout.Length;
        int c1 = m_FlattendMapLayoutRows;
        int c2 = m_FlattendMapLayoutCols;
        int c3 = count / (c1 * c2);
        mapLayout = new int[c1, c2,c3];
        for (int j = 0; j < c3; j++)
        {
            for (int i = 0; i < count; i++)
            {
                mapLayout[i % c1, i / c1,j] = m_FlattendMapLayout[i+(j+c1*c2)];
            }
        }
    }
}