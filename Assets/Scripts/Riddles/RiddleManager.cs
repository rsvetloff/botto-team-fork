﻿using UnityEngine;
using System.Collections;

public abstract class RiddleManager : MonoBehaviour
{
    /// <summary>
    /// the <seealso cref="GameObject"/> containing the game actioners\n
    /// the array index corresponds to the actioner id.
    /// </summary>
    public RiddleAction[] Actioners;
    /// <summary>
    /// The matching table between triger and actioners.
    /// </summary>
    public Int3DArray trigerActionMatcher;

    /// <summary>
    /// Recives a triger activation,
    /// </summary>
    /// <param name="trigerID"></param>
    protected abstract void reciveTriger(int trigerID);


}
