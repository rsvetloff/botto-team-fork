﻿using UnityEngine;
using System.Collections;
using System;
using UnityEditor;
using System.Linq;

public class Shotgun : Weapon {

    public int coneAngle;
    public int coneDensity;

    private ParticleSystem shotEffect;
    SerializedObject effect;
    void Start()
    {

        shotEffect = GetComponent<ParticleSystem>();
         effect = new SerializedObject(shotEffect);
        SerializedProperty it = effect.GetIterator();
        while (it.Next(true))
            Debug.Log(it.propertyPath);
        effect.FindProperty("ShapeModule.angle").floatValue = ((float)coneAngle)/2f;
        float particalspeed  = effect.FindProperty("InitialModule.startSpeed").floatValue;
        effect.FindProperty("InitialModule.startLifetime").floatValue = range/particalspeed;
        effect.ApplyModifiedProperties();

    }

    internal override void fire()
    {
        if(canFire())
        {
            
            
            GameObject[] hits = CustomeCasters.ConeCast2D(transform.position, aim.getMouseWorldPosition()-aim.transform.position ,coneAngle, range, coneDensity,int.MaxValue);
            HitResponder[] targets = new HitResponder[hits.Length];
            int j = -1;
            for (int i = 0;i<hits.Length;i++)
            {
                HitResponder hr = hits[i].GetComponentInParent<HitResponder>();
                if(hr!=null)
                {
                    j++;
                    targets[j] = hr;
                }
            }
             targets = targets.Distinct(new HitResponder.HitRespondEquator()).ToArray();
            HitEffect shotEffect = new HitEffect(HitEffectIndex.DAMAGE, damage);
            for (int i = 0; i < targets.Length; i++)
            {
                if(targets[i]!=null)
                {
                    targets[i].attributeHit(shotEffect);
                }
            }

            fireTimer = 1 / fireRate;
            fired = true;
        }
    }

    void Update()
    {
        if (fireTimer > 0)
        {
            fireTimer -= Time.deltaTime;
        }
    }

}
