﻿using UnityEngine;
using System.Collections;

public static class FloatUtils  {
	/// <summary>
	/// checks if a number is between two values inclusive.
	/// </summary>
	/// <param name="number">Number.</param>
	/// <param name="bottom">Bottom.</param>
	/// <param name="top">Top.</param>
	public static bool between(float number,float bottom,float top)
	{
		return (bottom <= number && number <= top);
	}
}
