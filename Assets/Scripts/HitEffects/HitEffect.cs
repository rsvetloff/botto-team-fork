﻿using UnityEngine;
using System.Collections;

public class HitEffect
{
    private HitEffectIndex _effectCode;

    public HitEffectIndex EffectCode
    {
        get
        {
            return _effectCode;
        }
    }

    private float _value;

    public float Value
    {
        get
        {
            return _value;
        }
    }

    public HitEffect(HitEffectIndex code ,float value)
    {
        _effectCode = code;
        _value = value;
    }

}
